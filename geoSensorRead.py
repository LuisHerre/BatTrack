import boto3
import json
import urllib2

def lambda_handler(event, context):
    # TODO implement
    # get list of first 10 devices to get data from
    # load the tables from dynamoDB
    resource = boto3.resource('dynamodb')
    name_table = resource.Table('geosensorname')
    
    # scan upto 10 devices from db for names
    scan_result = name_table.scan(Limit=10)
    names = scan_result['Items']
    
    # API Gateway url for geosensorByName. This may have to be done 
    # differently once the security token stuff gets set up.
    # I could not call the lambda function using invoke(), so this is 
    # a work around
    url = 'https://securityTokenGoesHere.execute-api.us-east-1.amazonaws.com/beta/geosensor/'
    
    # initialize response array
    response_array = []
    
    # for each device name, create the request url by concatenating
    # the device name onto the url. Then, call the API to build a response array
    # of the devices and their data
    for items in names:
        # build request url with device name
        request_url = url + items['sensorname']
        
        # get response json
        response = urllib2.urlopen(request_url)
        
        # append json to list of 10
        response_array.append(response.read())
    
    # create lambda_proxy response
    json_geosensor_array = json.dumps(response_array)
    response_json = {
    				"statusCode": 200,
    				"headers": {"Content-Type": "application/json"},
    				"body": json_geosensor_array
    }
    
    # return response json
    return response_json