from __future__ import print_function

import json
import boto3

print('Loading Create function')


def lambda_handler(event, context):

   # Dynamo stuff
   dynamodb = boto3.resource('dynamodb','us-east-1')
   geosensor_data = dynamodb.Table('geosensor')
   geosensor_name = dynamodb.Table('geosensorname')

   # unpack values from dictList. Still needs the geohash.
   for sensor_entry in event:
      sensor_name = sensor_entry["properties"]["sensorname"]
      sensor_timestamp = sensor_entry["properties"]["timestamp"]
      sensor_coordinates = [sensor_entry["geometry"]["coordinates"][0], sensor_entry["geometry"]["coordinates"][1]]
      dynamo_entry = {
    "coordinates": [
      str(sensor_coordinates[0]),
      str(sensor_coordinates[1])
    ],
    "sensorname": sensor_name,
    "timestamp": sensor_timestamp,
    "id": sensor_name + str(sensor_timestamp)
}
      geosensor_data.put_item(Item = dynamo_entry)
      geosensor_name.put_item(Item = {"sensorname": sensor_name})

   return {"status": "successful"}