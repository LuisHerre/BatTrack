rom __future__ import print_function # Python 2/3 compatibility
import boto3
import json
from boto3.dynamodb.conditions import Key, Attr

def lambda_handler(event, context):
    # load the tables from dynamoDB
    resource = boto3.resource('dynamodb')
    name_table = resource.Table('geosensorname')
    data_table = resource.Table('geosensor')
    
    # name of sensor requested
    #event_dict = json.loads(event)
    sensor_name = event['pathParameters']['sensorname']
    
    # check if sensor is in table
    response = name_table.query(
        KeyConditionExpression=Key('sensorname').eq(sensor_name)
    )
    if response['Count'] > 0:
        # sensor is in database
        # get data from sensor
        sensor_data = data_table.scan(FilterExpression=Key('sensorname').eq(sensor_name))
        sensor_data = sensor_data['Items']
        
        # make response json
        enriched_json = {
	                        "data": {
	                            "type": "Feature",
	                            "properties": {
	                                "sensor_name": sensor_name, # name of requested sensor
	                                "timestamp_array": [] # array of timestamps
	                            },
	                            "geometry": {
	                                "type": "LineString",
	                                "coordinates": [] # array of coordinates
	                                
	                            }
	                        }
        }
        
        # iterate through all the sensor_data entries
        # enrich response_json with sensor data
        for entry in sensor_data:
            timestamp = str(entry['timestamp'])
            coordinates = entry['coordinates']
            enriched_json['data']['properties']['timestamp_array'].append(timestamp)
            enriched_json['data']['geometry']['coordinates'].append(coordinates)
        
    else:
        # sensor is not in database
        # make response json
        enriched_json = {"Error": "Sensor Requested not in Database"}
    
    # escape the response_json
    enriched_escaped_json = json.dumps(enriched_json)
    
    # create lambda_proxy appropiate response json
    response_json = {
                "statusCode": 200, 
                "headers": {"Content-Type": "application/json"},
                "body": enriched_escaped_json
    }
    
    # return json
    return response_json