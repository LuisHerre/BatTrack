# BatTrack

   BatTrack implements a serverless API using AWS APIGateway and AWS Lambda. The Lambda fuctions implement the CRUD operations of an API. The goal is to have a device like a Rasberry pi with a GPS module
   that sends location data to the backend using GeoJSON. The backend can
   then send the tracker data to a web page that can display the tracker's
   route over time.

## Lambda functions

### geoSensorVersion
    https://{securityTokenGoesHere}.execute-api.us-east-1.amazonaws.com/beta/version
    Returns JSON with current version

### geoSensorWelcome
    https://{securityTokenGoesHere}.execute-api.us-east-1.amazonaws.com/beta/
    Returns JSON with welcome message

### geoSensorCreate
    Creates an entry for a new tracking sensor in DynamoDB

### geoSensorRead
    https://{securityTokenGoesHere}.execute-api.us-east-1.amazonaws.com/beta/geoSensor
    Reads the data of first 10 trackers in DynamoDB. Then returns an GeoJSON Array of the trackers.

### geoSensorByName
    https://{securityTokenGoesHere}.execute-api.us-east-1.amazonaws.com/beta/geoSensor/{NameOfSensor}
    Returns a GeoJSON with the data of the requested tracker name

### geoSensorDelete
    Under construction

## Data format

### GeoJSON

   The tracker data format is shown in singleSensor.json. The data is described using GeoJSON, so that the data can be ported easily to a frontend technology like Mapbox that can draw tracker coordinates on a map.